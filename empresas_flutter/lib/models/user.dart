class DataUser {
  final int id;
  final String token;
  final String client;
  final String uid;

  DataUser({this.id, this.token, this.client, this.uid});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["id"] = this.id;
    data["token"] = this.token;
    data["client"] = this.client;
    data["uid"] = this.uid;

    return data;
  }
}
