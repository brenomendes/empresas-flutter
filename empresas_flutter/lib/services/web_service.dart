import 'dart:convert';
import 'package:empresas_flutter/models/enterprise.dart';
import 'package:http/http.dart' as http;

class WebService {
  static const API = 'https://empresas.ioasys.com.br/api';
  static const VERSION = 'v1';
  static const ENTERPRISE_LOGIN_PATH = '/users/auth/sign_in';
  static const ENTERPRISE_SHOW_PATH = '/enterprises';

  Future<List<Map<String, dynamic>>> login({user, password}) async {
    Map<String, String> body = {"email": user, "password": password};
    final url = "$API/$VERSION/$ENTERPRISE_LOGIN_PATH";
    final response = await http.post(url, body: body);
    final bodyResponse = jsonDecode(response.body);
    final headersResponse = response.headers;

    List<Map<String, dynamic>> dataResponse = [];
    if (response.statusCode == 200) {
      dataResponse.add(bodyResponse);
      dataResponse.add(headersResponse);
    } else if (response.statusCode == 401) {
      dataResponse.add(bodyResponse);
    } else {
      dataResponse.add({'Error': 'unknown'});
    }
    return dataResponse;
  }

  Future<List<Enterprise>> fetchEnterprises({token, client, uid, search}) async {
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "access-token": token,
      "client": client,
      "uid": uid,
    };

    final url = "$API/$VERSION$ENTERPRISE_SHOW_PATH/?name=$search";
    final response = await http.get(url, headers: headers);

    if (response.statusCode == 200) {
      final body = jsonDecode(response.body);
      final Iterable json = body["enterprises"];
      return json.map((result) => Enterprise.fromJson(result)).toList();
    } else {
      throw Exception("Unable to perform request!");
    }
  }
}
