import 'package:empresas_flutter/pages/enterprises_search.dart';
import 'package:empresas_flutter/pages/login.dart';
import 'package:empresas_flutter/pages/splash.dart';
import 'package:empresas_flutter/views_models/enterprise_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Empresas Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Rubik',
      ),
      home: FutureBuilder(
        future: FlutterSession().get('myData'),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return SplashScreenWidget(
              screenDestiny: ChangeNotifierProvider(
                create: (context) => EnterpriseListViewModel(),
                child: EnterpriseSearchPage(),
              ),
            );
          } else {
            return SplashScreenWidget(
              screenDestiny: LoginPage(),
            );
          }
        },
      ),
    );
  }
}
