import 'package:empresas_flutter/services/web_service.dart';
import 'package:empresas_flutter/views_models/enterprise_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_session/flutter_session.dart';

class EnterpriseListViewModel extends ChangeNotifier {
  List<EnterpriseViewModel> enterprises = List<EnterpriseViewModel>();

  Future<void> fetchEnterprises({searchParameter}) async {
    dynamic userData = await FlutterSession().get('myData');
    final results = await WebService().fetchEnterprises(
      uid: userData['uid'],
      token: userData['token'],
      client: userData['client'],
      search: searchParameter,
    );

    this.enterprises = results
        .map((item) => EnterpriseViewModel(
              enterprise: item,
            ))
        .toList();
    notifyListeners();
  }
}
