import 'package:empresas_flutter/models/enterprise.dart';

class EnterpriseViewModel {
  final Enterprise enterprise;

  EnterpriseViewModel({this.enterprise});

  int get id {
    return this.enterprise.id;
  }

  String get name {
    return this.enterprise.name;
  }

  String get description {
    return this.enterprise.description;
  }

  String get email {
    return this.enterprise.email;
  }

  String get facebook {
    return this.enterprise.facebook;
  }

  String get twitter {
    return this.enterprise.twitter;
  }

  String get linkedIn {
    return this.enterprise.linkedIn;
  }

  String get photo {
    return this.enterprise.photo;
  }

  int get value {
    return this.enterprise.value;
  }

  int get shares {
    return this.enterprise.shares;
  }

  double get sharePrice {
    return this.enterprise.sharePrice;
  }

  bool get ownShares {
    return this.enterprise.ownEnterprise;
  }

  String get city {
    return this.enterprise.city;
  }

  String get country {
    return this.enterprise.country;
  }

  String get phone {
    return this.enterprise.phone;
  }

  Map<String, dynamic> get enterpriseType {
    return this.enterprise.enterpriseType;
  }
}
