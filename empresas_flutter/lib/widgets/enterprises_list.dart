import 'dart:math';
import 'package:empresas_flutter/pages/enterprise_detail.dart';
import 'package:empresas_flutter/views_models/enterprise_view_model.dart';
import 'package:flutter/material.dart';

class EnterpriseList extends StatelessWidget {
  final List<EnterpriseViewModel> enterprises;

  EnterpriseList({this.enterprises});

  Color colorRandom() {
    List<Color> hexColor = [
      Color(0xff79BBCA),
      Color(0xffEB9797),
      Color(0xff90BB81),
    ];
    var indexColor = Random().nextInt(3);
    return hexColor[indexColor];
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
          child: Text(
            this.enterprises.length.toString() + " resultados encontrados",
            style: TextStyle(fontWeight: FontWeight.w300, fontSize: 14, color: Color(0xff666666)),
          ),
        ),
        Expanded(
          child: ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: this.enterprises.length,
            itemBuilder: (context, index) {
              final enterprise = this.enterprises[index];

              return InkWell(
                onTap: () => Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) {
                    return EnterpriseDetail(
                      name: enterprise.name,
                      description: enterprise.description,
                    );
                  }),
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                  color: colorRandom(),
                  height: 120.0,
                  child: Center(
                    child: Text(
                      enterprise.name.toUpperCase(),
                      style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
