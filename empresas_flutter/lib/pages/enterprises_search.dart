import 'package:empresas_flutter/views_models/enterprise_list_view_model.dart';
import 'package:empresas_flutter/widgets/enterprises_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login.dart';

class EnterpriseSearchPage extends StatefulWidget {
  @override
  _EnterpriseSearchPageState createState() => _EnterpriseSearchPageState();
}

class _EnterpriseSearchPageState extends State<EnterpriseSearchPage> {
  final TextEditingController _controllerSearchForm = TextEditingController();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('myData');
  }

  @override
  void initState() {
    super.initState();
    Provider.of<EnterpriseListViewModel>(context, listen: false)
        .fetchEnterprises(searchParameter: '')
        .catchError((error, stackTrace) {
      logout();
      Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
        return LoginPage();
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<EnterpriseListViewModel>(context);

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      body: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Container(
          child: Column(
            children: [
              Container(
                height: 200,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xff8527bb),
                      Color(0xffb22a76),
                      Color(0xffce9cb8),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                height: 48,
                decoration: BoxDecoration(
                  color: Color(0xffF5F5F5),
                  borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
                ),
                child: TextFormField(
                  controller: _controllerSearchForm,
                  onChanged: (value) {
                    if (value.isNotEmpty) {
                      vm.fetchEnterprises(searchParameter: _controllerSearchForm.text);
                    }
                  },
                  style: TextStyle(color: Color(0xff666666), fontSize: 18),
                  decoration: InputDecoration(
                      hintText: "Pesquisa por empresa",
                      prefixIcon: Icon(
                        Icons.search,
                        color: Color(0xff666666),
                        size: 30,
                      ),
                      hintStyle: TextStyle(
                        color: Color(0xff666666),
                        fontSize: 18,
                      ),
                      border: InputBorder.none),
                ),
              ),
              Expanded(
                child: EnterpriseList(
                  enterprises: vm.enterprises,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
