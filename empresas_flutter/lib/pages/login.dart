import 'package:empresas_flutter/models/enterprise.dart';
import 'package:empresas_flutter/models/user.dart';
import 'package:empresas_flutter/pages/enterprises_search.dart';
import 'package:empresas_flutter/services/web_service.dart';
import 'package:empresas_flutter/views_models/enterprise_list_view_model.dart';
import 'package:empresas_flutter/widgets/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_session/flutter_session.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _loginInputController = TextEditingController();
  TextEditingController _passwordInputController = TextEditingController();

  TextStyle style = TextStyle(
    fontFamily: 'Rubik',
    fontSize: 16.0,
  );
  bool _obscureText = true;
  double _heightDynamic = 280.0;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _showLoginModal() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return LoadingCircular();
        });
  }

  void _authUser({user, password}) async {
    Map<String, dynamic> loginDataHeader;
    Map<String, dynamic> loginDataBody;

    if (_formKey.currentState.validate()) {
      _showLoginModal();
      List<Map<String, dynamic>> loginData = await WebService().login(user: user, password: password);
      if (loginData.isNotEmpty) {
        if (loginData.length > 1) {
          loginDataBody = loginData[0];
          loginDataHeader = loginData[1];
        } else {
          loginDataBody = loginData[0];
        }

        if (loginDataBody['success'] == true) {
          DataUser myData = DataUser(
            id: loginDataBody['investor']['id'],
            client: loginDataHeader['client'],
            token: loginDataHeader['access-token'],
            uid: loginDataHeader['uid'],
          );
          await FlutterSession().set('myData', myData);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_context) => ChangeNotifierProvider(
                create: (context) => EnterpriseListViewModel(),
                child: EnterpriseSearchPage(),
              ),
            ),
          );
        } else if (loginDataBody['success'] == false) {
          _scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text("Email/Senha incorretos, verifique e tente novamente!"),
              duration: Duration(seconds: 3),
              behavior: SnackBarBehavior.floating,
            ),
          );
        } else {
          _scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text("Ocorreu um erro, tente novamente!"),
              duration: Duration(seconds: 3),
              behavior: SnackBarBehavior.floating,
            ),
          );
        }
      }

      // Navigator.push(context, MaterialPageRoute(builder: (_context) => EnterpriseListPage()));
    } else {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Verifique os campos e tente novamente"),
          duration: Duration(seconds: 3),
          behavior: SnackBarBehavior.floating,
        ),
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    _loginInputController.dispose();
    _passwordInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: true,
      body: Container(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(bottom: 60),
              height: _heightDynamic,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.elliptical(350, 120),
                ),
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(0xff8527bb),
                    Color(0xffb22a76),
                    Color(0xffce9cb8),
                  ],
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    "assets/logo_icon.png",
                    width: 40,
                    height: 32,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 16.42),
                    child: Text(
                      "Seja bem vindo ao empresas!",
                      style: TextStyle(
                        fontSize: 20,
                        color: Color(0xffffffff),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              'Email',
                              style: TextStyle(color: Color(0XFF666666), fontSize: 14),
                            ),
                          ),
                          TextFormField(
                            controller: _loginInputController,
                            obscureText: false,
                            style: style,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
                              fillColor: Color(0xffF5F5F5),
                              border: OutlineInputBorder(),
                            ),
                            validator: (val) => val.isEmpty ? 'Campo vazio! Favor preencher' : null,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              'Senha',
                              style: TextStyle(color: Color(0XFF666666), fontSize: 14),
                            ),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            controller: _passwordInputController,
                            decoration: InputDecoration(
                              fillColor: Colors.amber,
                              contentPadding: EdgeInsets.symmetric(horizontal: 16.0),
                              border: OutlineInputBorder(),
                              suffix: GestureDetector(
                                onTap: () {
                                  _toggle();
                                },
                                child: Container(
                                  height: 17,
                                  width: 25,
                                  child: Stack(
                                    overflow: Overflow.visible,
                                    children: <Widget>[
                                      Positioned(
                                        child: Icon(
                                          _obscureText ? Icons.visibility : Icons.visibility_off,
                                          color: Color(0xFF184fa7),
                                          size: 24,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            validator: (val) => val.isEmpty ? 'Campo vazio! Favor preencher' : null,
                            obscureText: _obscureText,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 40),
                      child: Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(8.0),
                        color: Color(0xffE01E69),
                        child: MaterialButton(
                          minWidth: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          onPressed: () {
                            _authUser(user: _loginInputController.text, password: _passwordInputController.text);
                          },
                          child: Text(
                            "ENTRAR",
                            textAlign: TextAlign.center,
                            style: style.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
